from twiggy import log

from bugwarrior.services import IssueService
from bugwarrior.config import die

from gocept.support.rt import RT

class RTClient(object):

    def __init__(self, user, password, url, queue, owner):
        self.rt = RT(user, password, url, queue)
        self.owner = owner

    def fetch(self):
        tickets = self.rt.search(
            "(Status = 'new' OR Status = 'open') "
            "AND Owner = '{}'".format(self.owner))

        return tickets


class RTService(IssueService):
    def __init__(self, *args, **kw):
        super(RTService, self).__init__(*args, **kw)

        self.url = self.config.get(self.target, 'url').rstrip('/')
        user = self.config.get(self.target, 'user')
        password = self.config.get(self.target, 'password')
        queue = self.config.get(self.target, 'queue')
        if self.config.has_option(self.target, 'owner'):
            owner = self.config.get(self.target, 'owner')
        else:
            owner = user

        self.rtclient = RTClient(user, password, self.url, queue, owner)

    @classmethod
    def validate_config(cls, config, target):
        for k in ('url', 'user', 'password'):
            if not config.has_option(target, k):
                die("[%s] has no '%s'" % (target, k))

    def render_url(self, ticket):
        return self.url + "/Ticket/Display.html?id=" + str(ticket['id'])

    def issues(self):
        tickets_in = self.rtclient.fetch()

        tickets = []
        for ticket in tickets_in:
            if ticket.get('Status') == 'new':
                priority = 'H'
            else:
                priority = self.default_priority
            tickets.append(dict(
                description = self.description(
                    unicode(ticket['Subject'], 'utf8'),
                    self.render_url(ticket),
                    ticket['id'],
                    cls='rt'),
                project = ticket.get('CF.{report}', "-"),
                priority = priority))
        return tickets
